package broad;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import appService.AudioService;


public class MyAlarmReceiver extends BroadcastReceiver {

    public static final int REQUEST_CODE = 12345;
    //private boolean mBound = false;

    @Override
    public void onReceive(Context context, Intent intent) {

            intent = new Intent(context, AudioService.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setAction(AudioService.ACTION);
            //context.bindService(new Intent(context, AudioService.class), serviceConnection, Context.BIND_AUTO_CREATE);
            context.startService(intent);

    }

}