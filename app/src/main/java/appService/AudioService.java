package appService;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import broad.MyAlarmReceiver;

/**
 * Created by Salahuddin on 4/10/2017.
 */

public class AudioService extends Service implements MediaRecorder.OnInfoListener {

    private MediaRecorder mRecorder;
    private long mStartTime;
    private File mOutputFile;
    private MyAlarmReceiver myAlarmReceiver;
    public final static String ACTION = "AudioServiceAction";

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        Thread thread = new Thread(new MyThreadClass(startId));
        thread.start();

        return Service.START_STICKY;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        //startRecording();

        myAlarmReceiver = new MyAlarmReceiver();
        IntentFilter intentFilter = new IntentFilter("com.app.ACTION_ONE");
        intentFilter.addAction(ACTION);
        registerReceiver(myAlarmReceiver, intentFilter);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /*private void startRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setOnInfoListener(this);
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        //mRecorder.setMaxFileSize(Audio_MAX_FILE_SIZE);
        mRecorder.setMaxDuration(1800000);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.HE_AAC);
            mRecorder.setAudioEncodingBitRate(48000);
        } else {
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            mRecorder.setAudioEncodingBitRate(64000);
        }
        mRecorder.setAudioSamplingRate(16000);
        mOutputFile = getOutputFile();

        mOutputFile.getParentFile().mkdirs();
        mRecorder.setOutputFile(mOutputFile.getAbsolutePath());

        try {
            mRecorder.prepare();
            mRecorder.start();
            mStartTime = SystemClock.elapsedRealtime();

        } catch (IOException e) {
        }
    }*/

    private void startRecording() {
        mRecorder = new MediaRecorder();

        mRecorder.setOnInfoListener(this);
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setMaxDuration(1800000);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.HE_AAC);
           // mRecorder.setAudioEncodingBitRate(48000);
            mRecorder.setAudioEncodingBitRate(24000);
        } else {
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            //mRecorder.setAudioEncodingBitRate(64000);
            mRecorder.setAudioEncodingBitRate(32000);
        }
        //mRecorder.setAudioSamplingRate(16000);
        mRecorder.setAudioSamplingRate(8000);
/*
        CamcorderProfile cpHigh = CamcorderProfile.get(CamcorderProfile.QUALITY_480P);
        mRecorder.setProfile(cpHigh);*/

        mOutputFile = getOutputFile();
        mOutputFile.getParentFile().mkdirs();

        mRecorder.setOutputFile(mOutputFile.getAbsolutePath());


        try {
            mRecorder.prepare();
            mRecorder.start();
            mStartTime = SystemClock.elapsedRealtime();

        } catch (IOException e) {
        }
    }

    protected void stopRecording(boolean saveFile) {
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
        mStartTime = 0;
      /*  if (!saveFile && mOutputFile != null) {
            mOutputFile.delete();
        }*/

        // to stop the service by itself
       // stopSelf();
        // Again record
        startRecording();
    }

    private File getOutputFile() {
           SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmm");

          return   new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                    + "/Voice Recorder/file_"
                    + dateFormat.format(new Date())
                    + ".3gp");

    }

  /*  private File getOutputFile() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmm");
        return new File(Environment.getExternalStorageDirectory().getAbsolutePath().toString()
                + "/Voice Recorder/file_"
                + dateFormat.format(new Date())
                + ".m4a");
    }*/


    @Override
    public void onInfo(MediaRecorder mr, int what, int extra) {
        //check whether file size has reached to 1MB to stop recording
        if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
            stopRecording(true);
        }
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(myAlarmReceiver);
        super.onDestroy();
        stopRecording(true);
    }

    final class  MyThreadClass implements Runnable{
        private int start_id;

        public MyThreadClass(int start_id){
            this.start_id =start_id;
        }

        @Override
        public void run() {
            startRecording();
        }
    }

    private static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }

}